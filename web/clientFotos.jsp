<%-- 
    Document   : clientFotos
    Created on : 27-abr-2018, 19:18:26
    Author     : dawa52
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es"><head>
	<meta name="google-site-verification" content="b5Njx5z_KsMWgJdT6RRF-Q47KhmY4xYmThV3i1YFPMU" />
	
	<meta charset="utf-8">
	<link href="styles.css" type="text/css" rel="stylesheet">
	<link rel="stylesheet" href="normalize.css" />
    <link rel="icon" type="image/jpg"  href="Galeria/kisspng-computer-icons-logo-social-media-instargram-5b559513cfc348.931170821532335379851.jpg" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" 
    integrity=	"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">




    
	<meta property="og:locale" content="es_ES" />
	<meta property="og:type" content="website" />
    <title>VegaImaxe - Fotografo de A Coruna</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="VegaImaxe - Fotografo, bodas, bautizos, Comuniones, books, niños, eventos." />
    <meta name="DC.Title" content="VegaImaxe" />
    <meta http-equiv="title" content="VegaImaxe" />
    <meta name="keywords" content="VegaImaxe, estudio, fotografia, A Coruña, Galicia, bodas, eventos, books, niños, bautizos, Comuniones, foto, fotoreportaje, fotografos" />
    <meta name="description" content="VegaImaxe Fotografo de A Coruña - Bodas, bautizos, Comuniones, niños, books, eventos." />
    <meta name="author" content="Eduardo Moleon v" />
    <meta name="DC.Creator" content="Eduardo Moleon v" />
    <meta http-equiv="description" content="VegaImaxe Fotografo de A Coruña - Bodas, bautizos, Comuniones, ninos, books, eventos." />
    <meta http-equiv="DC.Description" content="VegaImaxe Fotografo de A Coruña - Bodas, bautizos, Comuniones, ninos, books, eventos." />
    <meta name="VW96.objecttype" content="Document">
    <meta http-equiv="" content="text/html; charset=iso-8859-1">
    <meta name="DC.Language" scheme="RFC1766" content="Spanish">
    <meta name="distribution" content="global">
    <meta name="resource-type" content="document">
    <meta http-equiv="Pragma" content="cache">
    <meta name="Revisit" content="30 days">
    <meta name="robots" content="all">
    
	<style>
        body {
            min-height: 100vh !important;
        }
	@import url('https://fonts.googleapis.com/css?family=Merienda');
    .espacioParallax{
		height:230px;
		background-image:url(Galeria/FondoPrincipalLadrillo.jpg);
		background-repeat: repeat;
		background-position: center 0%;
			
	}
	@media screen and (max-width:500px){
		#logo {
			width: 120px !important;
		}
		.espacioParallax{
			height:150px;
		}
		
	}
	
	article ul.nav-pills {
	  top: 130px;
	  position: fixed;
	}
	@media screen and  (max-width:650px) {
		article .nav-pills > li > a{
			color: black !important;
			font-size: 8px;
		}
		article .nav-pills > li > a.active {
		  background-color: #21618C !important;
		  color: #F7DC6F !important;
		  font-size: 8px;
		}
	}
	@media screen and (min-width:651px) {
		article .nav-pills > li > a{
			color: black !important;
			font-size: 13px;
		}
		article .nav-pills > li > a.active {
		  background-color: #21618C !important;
		  color: #F7DC6F !important;
		  font-size: 13px;
		}
	}
	
	#myText{
		background-color: #075E54 !important;
		color: #FFFFFF;
		font-weight: bold;
		text-align: center;
	}
	li{
	list-style-type: "decimal";
	text-align: left !important;
	
	}
	.espacioD{
		padding-left: 5%;
	}
    </style>

</head>
<body>
	<header>
      <nav class="navbar navbar-expand-lg navbar-light navbar-default scrolled fixed-top">
      <a class="navbar-brand" href="#"><img id="logo" class="img-fluid" src="Galeria/LOGO2.png" alt="logo" style="width:190px;"/></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" 
      aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample05">
        <ul class="navbar-nav">
          <li class="nav-item active">
                	<a class="nav-link t3" href="index.html">Inicio <span class="sr-only">(current)</span></a>
                    
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle t3" href="Album.html" id="dropdown06" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Album</a>
                     <div class="dropdown-menu" aria-labelledby="dropdown06">
                      <a class="dropdown-item" href="Album.html">Boda</a>
                      <a class="dropdown-item" href="Album1.html">Book</a>
                      <a class="dropdown-item" href="Album10.html">Embarazada</a>
                      <a class="dropdown-item" href="Album2.html">Fot.Estudio</a>
                      <a class="dropdown-item" href="Album3.html">Paisajes</a>
                      <a class="dropdown-item" href="Album8.html">Reparación</a>
                      <a class="dropdown-item" href="Album4.html">Reportaje</a>
                      <a class="dropdown-item" href="Album6.html">Eventos</a>
                      <a class="dropdown-item" href="Album7.html">Mascotas</a>
                      <a class="dropdown-item" href="Album5.html">Reproducción</a>
                    </div>
                </li>
                <li class="nav-item">
                	<a class="nav-link t3" href="Tarifas.html">Tarifas</a>
                </li>
                <li class="nav-item">
                	<a class="nav-link t3" href="Curriculum.html">Currículum</a>
                </li>
                <li class="nav-item">
                	<a class="nav-link t3" href="Contacto.html">Contacto</a>
                </li>
            </ul>
        </div>
    </nav> 
	
    <!--Parallax-->
    <div class="col-sm-12 col-md-12 espacioParallax">

    </div>
    <div class="jumbotron"
         style="background: url('Galeria/paroller1.jpg') no-repeat center;
            background-size: cover;"
            data-paroller-factor="0.5"
            data-paroller-factor-xs="0.2"
    >
        <p>Datos Form</p>
    </div>
    <!--Fin-->
	            
			
		
                

</header>
<div class="container-fluid" Style="margin-top: -16px">
	<section class="row Principal">
        <div class="col-sm-12 col-md-10 col-lg-6 mx-auto">   
            <div class="TextoSection">
                <h3 class="text-center" Style="padding-top: 180px">Eduardo Moleón Vega - Fotógrafo</h3>
                <br>
                <div class="poster" Style="margin-top: 50px">
                    <h1>${message}</h1>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">DNI</th>
                                    <th scope="col">NOMBRE</th>
                                    <th scope="col">TELEFONO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">${cliente.id}</th>
                                    <td>${cliente.dni}</td>
                                    <td>${cliente.nombre}</td>
                                    <td>${cliente.telefono}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">EVENTO</th>
                                    <th scope="col">DESCRIPCION</th>
                                    <th scope="col">LUGAR</th>
                                    <th scope="col">FECHA</th>
                                    <th scope="col">HORA</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <c:forEach var="foto" items="${cliente.fotos}">
                                            <c:out value="${foto.evento}"/>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:forEach var="foto" items="${cliente.fotos}">
                                            <c:out value="${foto.descripcion}"/>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:forEach var="foto" items="${cliente.fotos}">
                                            <c:out value="${foto.lugar}"/>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:forEach var="foto" items="${cliente.fotos}">
                                            <c:out value="${foto.fecha}"/>
                                        </c:forEach>
                                    </td>
                                    <td>
                                        <c:forEach var="foto" items="${cliente.fotos}">
                                            <c:out value="${foto.hora}"/>
                                        </c:forEach>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
	</div>  
    </section>   
    <footer>
        <nav class="row navbar navbar-expand-lg navbar-dark bg-dark justify-content-center">
            <span class="navbar-brand">Sígueme!</span>
            <a href="https://www.facebook.com/fotoedu.fotografia">
                <img src="Galeria/face.png" width="50" height="50" alt="" />
            </a>
            <a href="">
                <img src="Galeria/Instagram.png" width="38" height="38" alt=""/>
            </a>
            <a href="http://www.youtube.com/user/EduardoMoleonVega/videos">
                <img src="Galeria/youtube.png" width="48" height="48" alt=""/>
                        </a>
            <a class="nav-link" href="">fotoedu@yahoo.es 620 572 550</a>
        </nav>
    </footer>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="jquery.paroller.js"></script>
<script src="jquery.paroller.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="https://afernandezgarcia.com/codepen/carousel/js/touchswipe.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<script>
$(document).ready(function() {
	
	$('.jumbotron').paroller();
	// popovers Initialization
	$(function () {
		$('[data-toggle="popover"]').popover();
	});
	
});
</script>
</body>
</html>
