<%-- 
    Document   : addAlquiler
    Created on : 18-may-2018, 20:26:54
    Author     : dawa52
--%>

%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/fotoCCS.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="fondo">
        <h1>Introduce Foto</h1>
        <h1>${message}</h1>
        <form action="./AddAlquiler">
            <table border="1">
                <tbody>
                    <tr>
                        <th id="cabeza">ID</th>
                        <td>
                            <strong>${cliente.id}</strong>
                            <input type="hidden" name="id" value="${cliente.id}"/>
                        </td>
                    </tr>
                    <tr>
                        <th id="cabeza">NOMBRE</th>
                        <td><strong>${cliente.nombre}</strong></td>
                    </tr>
                    <tr>
                        <th id="cabeza">CAMARA</th>
                        <td><input type="text" name="camara" value="" size="20" required/></td>
                    </tr>
                    <tr>
                        <th id="cabeza">TIPO</th>
                        <td><input type="text" name="tipo" value="" size="150" required/></td>
                    </tr>
                </tbody>
            </table>
            <input type="submit" value="REGISTER" />
        </form>
        <h2><a href="./index.jsp">Index</a></h2>
        </div>
    </body>
</html>
