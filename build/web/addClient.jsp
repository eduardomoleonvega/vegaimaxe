<%-- 
    Document   : addClient
    Created on : 27-abr-2018, 18:36:33
    Author     : dawa52
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/fotoCCS.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="fondo">
        <h1>INTRODUCE CLIENTE Y FOTO</h1>
        <form action="./AddClient">
            <table border="1">
                <tbody>
                    <tr>
                        <th id="cabeza">CLIENTE</th>
                    </tr>
                    <tr>
                        <th id="cabeza">DNI</th>
                        <td><input type="text" name="dni" value="" size="20" required=""/></td>
                    </tr>
                    <tr>
                        <th id="cabeza">NOMBRE</th>
                        <td><input type="text" name="nombre" value="" size="20" required=""/></td>
                    </tr>
                    <tr>
                        <th id="cabeza">TELEFONO</th>
                        <td><input type="text" name="telefono" value="" size="20" required=""/></td>
                    </tr>
                    <tr>
                        <th id="cabeza">EVENTO</th>
                        <td><input type="text" name="evento" value="" size="20" required/></td>
                    </tr>
                    <tr>
                        <th id="cabeza">DESCRIPCION</th>
                        <td><input type="text" name="descripcion" value="" size="150" required/></td>
                    </tr>
                    <tr>
                        <th id="cabeza">LUGAR</th>
                        <td><input type="text" name="lugar" value="" size="20" required/></td>
                    </tr>
                    <tr>
                        <th id="cabeza">FECHA</th>
                        <td><input type="text" name="fecha" value="" size="20" required/></td>
                    </tr>
                    <tr>
                        <th id="cabeza">HORA</th>
                        <td><input type="text" name="hora" value="" size="20" required/></td>
                    </tr>
                </tbody>
            </table>
            <input type="submit" value="REGISTER" />
        </form>
        </div>
    </body>
</html>
