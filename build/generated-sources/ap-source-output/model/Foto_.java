package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-11T01:53:26")
@StaticMetamodel(Foto.class)
public class Foto_ { 

    public static volatile SingularAttribute<Foto, String> descripcion;
    public static volatile SingularAttribute<Foto, String> fecha;
    public static volatile SingularAttribute<Foto, String> evento;
    public static volatile SingularAttribute<Foto, String> hora;
    public static volatile SingularAttribute<Foto, String> lugar;
    public static volatile SingularAttribute<Foto, Integer> id;

}