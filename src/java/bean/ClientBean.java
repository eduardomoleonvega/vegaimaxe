/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Cliente;
import model.Foto;

/**
 *
 * @author dawa52
 */
@Stateless
public class ClientBean {
    @PersistenceContext (unitName = "VegaImaxePU")
    EntityManager em;
    
    public Cliente addClient (String dni, String nombre, String telefono, 
            String evento, String descripcion, String lugar, String fecha, String hora) {  
        Cliente cliente = new Cliente();
        Foto foto = new Foto(evento,  descripcion, lugar, fecha, hora);
        ArrayList fotos;
        fotos = cliente.getFotos();
        if (fotos == null) {
            fotos = new ArrayList<>();
            fotos.add(foto);
        } else {
            fotos.add(foto);
        }
        
        cliente.setDni(dni);
        cliente.setNombre(nombre);
        cliente.setTelefono(telefono);
        cliente.setFotos(fotos);
        
        
        em.persist(cliente);
        em.persist(foto);
        
        return cliente;
    }
    
    public List<Cliente> viewClients () {
        Query q = em.createQuery("SELECT c FROM Cliente c");
        List<Cliente> clients = q.getResultList();
        return clients;
    }
}

